package chg.httpanaloginterface;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HttpAnalogInterfaceApplication {
    public static void main(String[] args) {
        SpringApplication.run(HttpAnalogInterfaceApplication.class, args);
    }
}
