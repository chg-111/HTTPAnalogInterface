package chg.httpanaloginterface.service;

import com.alibaba.fastjson.JSON;
import chg.httpanaloginterface.utils.JsonUtil;
import org.springframework.stereotype.Service;

import javax.annotation.PreDestroy;
import java.io.*;
import java.util.*;

@Service
public class JsonServe {
    private List<JsonUtil> jsonServeList = new ArrayList<>();
    private File file;
    private String urlPath;
    String projectPath;

    //初始化
    {
        try {//参数为空
            projectPath = new File("").getCanonicalPath();
            String str;
            file = new File(projectPath+"/http.json");
            if (file.exists()){
                str = this.readFileByFile(file);
            }else{
                str = "[{\"url\":\"http\",\"jsonPara\":[\"para1\",\"para2\",\"para3\"],\"json\"" +
                        ":[{\"para1\": \"data1\",\"para2\": \"data2\",\"para3\": \"data3\"}]}]";
            }
            jsonServeList = JSON.parseArray(str,JsonUtil.class);
            if (jsonServeList == null){
                jsonServeList = new ArrayList<>();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //添加URL
    public String addJsonUtil(JsonUtil jsonUtil){
        System.out.println("新路径为:" + urlPath + "/data/"+jsonUtil.getUrl());
        if (getJsonUtilByURL(jsonUtil.getUrl())!=null){
            deleteJsonUtil(jsonUtil.getUrl());
            jsonServeList.add(jsonUtil);
            return "已重置链接:" + jsonUtil.getUrl();
        }else{
            jsonServeList.add(jsonUtil);
            return "链接" + jsonUtil.getUrl() + "添加成功";
        }
    }

    //删除URL
    public String deleteJsonUtil(String jsonURL){
        boolean b = jsonServeList.removeIf(jsonUtil1 -> jsonUtil1.getUrl().equals(jsonURL));
        if (b){
            System.out.println("成功删除链接:"+jsonURL);
            return "成功删除链接:"+jsonURL;
        }
        else
            return jsonURL+"的URL链接不存在!";
    }

    //查看URL
    public String getJSONPara(String jsonURL){
        if (getJsonUtilByURL(jsonURL)!=null){
            return getJsonUtilByURL(jsonURL).getJsonPara().toString();
        }
        else{
            return jsonURL+"的URL链接不存在!";
        }
    }

    //添加JSON对象
    public String addJSON(String jsonURL,Map<String,String> map){
        return getJsonUtilByURL(jsonURL).addJSON(map);
    }

    //删除JSON对象
    public String deleteJSON(String jsonURL,String field,String value){
        return getJsonUtilByURL(jsonURL).deleteJSON(field,value);
    }

    //修改JSON对象
    public String updateJSON(String jsonURL,String field,Map<String,String> map){
        return getJsonUtilByURL(jsonURL).updateJSON(field,map);
    }

    //查看JSON对象
    public List<Map> selectJSON(String jsonURL,String field,String value){
        if (field == null || value == null){
            return getJsonUtilByURL(jsonURL).getJson();
        }else{
            return getJsonUtilByURL(jsonURL).getJSONByField(field,value);
        }
    }

    //通过文件名字获取文件内容
    public String readFileByFile(File file){
        StringBuilder stringBuilder = new StringBuilder();
        BufferedReader bufferedReader;
        try {
            bufferedReader = new BufferedReader(new FileReader(file));
            while (bufferedReader.ready()) {
                stringBuilder.append(bufferedReader.readLine());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return String.valueOf(stringBuilder);
    }

    //抛出路径异常
    public List<Map> throwError(){
        List<Map> list = new ArrayList<>();
        Map map = new HashMap<>();
        map.put("路径异常","您的路径出错了");
        list.add(map);
        return list;
    }

    //输出请求路径
    public void outputURL(String urlPath){
        this.urlPath = urlPath;
        System.out.println("---------------------------------------------------");
        System.out.println("源文件路径:"+projectPath+"\\http.json");
        System.out.println("增加URL路径,请在浏览器中打开链接:"+urlPath);
        System.out.println("---------------------------------------------------");
        if (jsonServeList.size() > 0) {
            System.out.println("url请求路径:");
            for (JsonUtil jsonUtil : jsonServeList) {
                System.out.println(urlPath+"/data/" + jsonUtil.getUrl());
            }
        }else{
            System.out.println("当前无请求路径!");
        }
        System.out.println("---------------------------------------------------");
    }

    //对象销毁时保存JSON对象
    @PreDestroy
    protected void finalize(){
        BufferedWriter bufferedWriter = null;
        try {
            bufferedWriter  = new BufferedWriter(new FileWriter(this.file));
            bufferedWriter.write(JSON.toJSONString(this.jsonServeList));
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                assert bufferedWriter != null;
                bufferedWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        System.out.println("此次数据已保存!");
    }

    //通过路径获取JsonUtil对象
    public JsonUtil getJsonUtilByURL(String jsonURL){
        for (JsonUtil jsonUtil : jsonServeList) {
            if (Objects.equals(jsonUtil.getUrl(), jsonURL)) {
                return jsonUtil;
            }
        }
        return null;
    }
}
