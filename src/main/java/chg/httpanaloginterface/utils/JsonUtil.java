package chg.httpanaloginterface.utils;
import com.alibaba.fastjson.annotation.JSONField;

import java.util.*;

public class JsonUtil {

    /**
     * 链接接口名称
     */
    private String url;
    /**
     * JSON数据参数列表
     */
    private List<String> jsonPara;
    /**
     * JSON数据列表
     */
    private List<Map> json;

    //初始化json
    {
        json = new ArrayList<>();
    }

    public void setUrl(String url) {this.url = url;}
    public void setJson(List<Map> json) {this.json = json;}
    public void setJsonPara(List<String> jsonPara) {this.jsonPara = jsonPara;}

    @JSONField(name = "url",ordinal = 1)
    public String getUrl(){return url;}

    @JSONField(name = "jsonPara",ordinal = 2)
    public List<String> getJsonPara() {return jsonPara;}

    @JSONField(name = "json",ordinal = 3)
    public List<Map> getJson(){return json;}

    //通过给定字段查询所有json数据
    public List<Map> getJSONByField(String field,String value){
        if (isExistField(field)) return new ArrayList<>();

        List<Map> listJSON = new ArrayList<>();
        for (Map map: json) {
            if (map.get(field).equals(value)){
                listJSON.add(map);
            }
        }
        return listJSON;
    }

    //修改json数据
    public String updateJSON(String field,Map<String,String> mapJSON){
        if (isExistField(field)) return field + "字段不存在";
        if (!mapJSON.containsKey(field)) return "请求体中不存在字段"+field;

        Iterator<Map> iter = json.iterator();
        for (int i = 0;i < json.size() ; i++){
            Map map = iter.next();
            if (map.get(field).equals(mapJSON.get(field))){
                Set<String> sets = mapJSON.keySet();
                for (String obj : sets) {
                    if (jsonPara.contains(obj)){
                        map.put(obj,mapJSON.get(obj));
                    }else{
                        return "JSON格式异常";
                    }
                }
                json.set(i,map);
            }
        }
        return "更新完成";
    }

    //添加json数据
    public String addJSON(Map<String,String> mapJSON){
        Set<String> sets = mapJSON.keySet();
        for (String obj : sets) {
            if (!jsonPara.contains(obj)){
                return "JSON格式异常";
            }
        }
        json.add(mapJSON);
        return "添加成功";
    }

    //删除json数据
    public String deleteJSON(String field,String value){
        if (isExistField(field)) return field + "字段不存在";

        json.removeIf(map -> map.get(field).equals(value));
        return "删除成功";
    }

    //判断字段是否存在
    public boolean isExistField(String field){
        if (json.size() > 0){
            return !json.get(0).containsKey(field);
        }
        return true;
    }
}
