package chg.httpanaloginterface.config;

import chg.httpanaloginterface.HttpAnalogInterfaceApplication;
import chg.httpanaloginterface.service.JsonServe;
import io.github.yedaxia.apidocs.Docs;
import io.github.yedaxia.apidocs.DocsConfig;
import io.github.yedaxia.apidocs.plugin.markdown.MarkdownDocPlugin;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;

/**
 * 自动打开浏览器
 */
@Configuration
public class ConfigItem implements CommandLineRunner{

    @Value("${isOpen}")
    private boolean isOpen;

    @Value("${server.port}")
    private String port;

    @Value("${server.servlet.context-path}")
    private String url;

    @Resource
    JsonServe jsonServe;

    public String getURLPath(){
        return "http://localhost:" + port + url;
    }


    @Override
    public void run(String... args){
        jsonServe.outputURL(getURLPath());
        openBrowser();
        createDocument();
    }

    //是否打开浏览器
    private void openBrowser(){
        if(isOpen){
            String runCmd = "cmd   /c   start " + getURLPath() ;
            Runtime run = Runtime.getRuntime();
            try {
                run.exec(runCmd);
                System.out.println("启动浏览器打开项目成功");
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("启动项目自动打开浏览器失败");
            }
        }
    }

    //自动生成项目文档
    private void createDocument(){
        DocsConfig config = new DocsConfig();
        config.setProjectPath("C:/DAMA/project/HTTPAnalogInterface"); // root project path
        config.setProjectName("HTTP接口文档"); // project name
        config.setApiVersion("api");       // api version
        config.setDocsPath("C:/DAMA/project/HTTPAnalogInterface/src/main/resources/html"); // api docs target path
        config.setAutoGenerate(Boolean.TRUE);  // auto generate
        Docs.buildHtmlDocs(config); // execute to generate
    }
}
