package chg.httpanaloginterface.controller;

import chg.httpanaloginterface.config.ConfigItem;
import chg.httpanaloginterface.service.JsonServe;
import chg.httpanaloginterface.utils.JsonUtil;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * 链接管理接口
 */
@Controller
public class Index {
    @Resource
    ConfigItem configItem;
    @Resource
    JsonServe jsonServe;

    /**
     * 链接管理页面
     * @return index.html
     */
    @RequestMapping("/")
    public String index(Model model, HttpServletRequest request){
        model.addAttribute("urlPath",configItem.getURLPath());
        return "index";
    }

    /**
     * 添加链接接口
     * @param jsonUtil 链接对象
     * @return 添加结果
     */
    @ResponseBody
    @PostMapping("/json")
    public String additionJSON(@RequestBody JsonUtil jsonUtil){
        return jsonServe.addJsonUtil(jsonUtil);
    }

    /**
     * 删除链接接口
     * @param url 接口名称
     * @return 删除结果
     */
    @ResponseBody
    @DeleteMapping("/json/{url}")
    public String removeJSON(@PathVariable("url") String url){
        return jsonServe.deleteJsonUtil(url);
    }

    /**
     * 查询链接接口
     * @param url 接口名称
     * @return 单个链接的所有参数名称
     */
    @ResponseBody
    @GetMapping("/json/{url}")
    public String selectJSON(@PathVariable("url") String url){
        return jsonServe.getJSONPara(url);
    }
}
