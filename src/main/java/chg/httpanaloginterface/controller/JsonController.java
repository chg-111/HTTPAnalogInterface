package chg.httpanaloginterface.controller;

import chg.httpanaloginterface.service.JsonServe;
import chg.httpanaloginterface.utils.JsonUtil;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.*;

/**
 * JSON操作接口
 */
@RestController
@CrossOrigin
public class JsonController {
    @Resource
    JsonServe jsonServe;

    /**
     * 查询所有JSON对象
     * @description 查询接口对应的所有JSON对象数组
     * @param jsonURL 接口名称
     * @return JSON数组
     */
    @GetMapping("/data/{jsonURL}")
    public List<Map> getFileData(@PathVariable("jsonURL") String jsonURL){
        if (jsonServe.getJsonUtilByURL(jsonURL) != null){
            return jsonServe.selectJSON(jsonURL,null,null);
        }
        return jsonServe.throwError();
    }

    /**
     * 查询部分JSON对象
     * @description 根据字段和对应的值查询匹配的JSON对象
     * @param jsonURL 接口名称
     * @param field JSON对象中的参数
     * @param value 参数所对应的值
     * @return 查询到的JSON对象
     */
    @GetMapping("/data/{jsonURL}/{field}/{value}")
    public List<Map> getFileDataByField(@PathVariable("jsonURL") String jsonURL,
                                        @PathVariable("field") String field,
                                        @PathVariable("value") String value){
        if (jsonServe.getJsonUtilByURL(jsonURL) != null){
            return jsonServe.selectJSON(jsonURL,field,value);
        }
        return jsonServe.throwError();
    }

    /**
     * 添加JSON数据
     * @param jsonURL 接口名称
     * @param map 需要添加的JSON对象
     * @return 添加结果
     */
    @PostMapping("/data/{jsonURL}")
    public String addFileData(@PathVariable("jsonURL") String jsonURL,
                              @RequestBody Map<String,String> map){
        if (jsonServe.getJsonUtilByURL(jsonURL) != null){
            return jsonServe.addJSON(jsonURL,map);
        }
        return jsonURL + "路径不存在!";
    }

    /**
     * 修改JSON数据
     * @param jsonURL 接口名称
     * @param field JSON对象中的参数
     * @param map 需要修改的JSON数据对象
     * @return 修改结果
     */
    @PutMapping("/data/{jsonURL}/{field}")
    public String updateFileDataByField(@PathVariable("jsonURL") String jsonURL,
                                        @PathVariable("field") String field,
                                        @RequestBody Map<String,String> map){
        if (jsonServe.getJsonUtilByURL(jsonURL) != null){
            return jsonServe.updateJSON(jsonURL,field,map);
        }
        return jsonURL + "路径不存在!";
    }

    /**
     * 删除JSON对象
     * @param jsonURL 接口名称
     * @param field JSON对象中的参数
     * @param value 参数所对应的值
     * @return 删除结果
     */
    @DeleteMapping("/data/{jsonURL}/{field}/{value}")
    public String deleteFileDataByField(@PathVariable("jsonURL") String jsonURL,
                                        @PathVariable("field") String field,
                                        @PathVariable("value") String value){
        if (jsonServe.getJsonUtilByURL(jsonURL) != null){
            return jsonServe.deleteJSON(jsonURL,field,value);
        }
        return jsonURL + "路径不存在!";
    }
}
