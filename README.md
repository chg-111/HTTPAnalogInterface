# 配置

- 默认端口号(port):5201
- 默认全局路径(URL):jsonURL
- 默认打开浏览器:true


关闭浏览器自动打开

```cmd
java -jar HTTPAnalogInterface-0.0.1-SNAPSHOT.jar --isOpen=false
```

启动时修改端口号(如:5203)

```cmd
java -jar HTTPAnalogInterface-0.0.1-SNAPSHOT.jar --server.port=5203
```

启动时修改全局路径(如:/java)

```cmd
java -jar HTTPAnalogInterface-0.0.1-SNAPSHOT.jar --server.servlet.context-path=/java
```

启动时既修改端口号又修改全局路径(如端口:5203,路径:/java)

```cmd
java -jar HTTPAnalogInterface-0.0.1-SNAPSHOT.jar --server.port=5203 --server.servlet.context-path=/java
```


# URL

## 添加URL链接(POST方法)

```http request
http://localhost:port/URL/json
```

## 删除URL链接(DELETE方法)

```http request
http://localhost:port/URL/json/{jsonURL}
```

## 查看URL链接(GET方法)

```http request
http://localhost:port/URL/json/{jsonURL}
```

## 一个URL链接的基本json格式

```json
{
  "url":"",//链接名称,
  "jsonPara":[],//json对象的所有字段
  "json": [//数据的存放区域
    {},//单个JSON对象
    {},
    {},
    ...
  ]
}
```

# 请求的URL地址概述

```http request
http://localhost:port/URL/data/{jsonURL}/{field}/{value}
```

- {fileURL}:上传的url链接名称
- {field}:json对象中的字段名
- {value}:字段对应的值

# 单个URL的请求方式

- 查询所有数据
    - get
    - 返回类型 json
    - http://localhost:port/URL/data/{jsonURL}


- 通过字段和对应值查询数据
    - get
    - 返回类型 json
    - http://localhost:port/URL/data/{jsonURL}/{field}/{value}


- 添加数据(请确保添加的数据字段和初始给定的字段一致)
    - post
    - 返回类型 String
    - 请求类型 JSON
    - http://localhost:port/URL/data/{jsonURL}

- 根据给定的字段修改数据
    - put
    - 返回类型 String
    - 请求类型 JSON
    - http://localhost:port/URL/data/{jsonURL}/{field}


- 通过字段和对应值删除fileURL文件中的数据
    - delete
    - 返回类型 String
    - http://localhost:port/URL/data/{jsonURL}/{field}/{value}

# 注

- 请勿直接创建json文件,系统会自动在jar包路径下创建json文件
- 请勿直接修改json文件,修改后系统可能出现异常问题
- 请勿在json文件中手动添加json数据,手动添加可能出现参数异常
- 如果出现报错现象,请删除根目录下的json文件,然后重启